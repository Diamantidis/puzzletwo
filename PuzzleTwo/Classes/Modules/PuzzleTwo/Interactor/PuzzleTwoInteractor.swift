//
//  PuzzleTwoInteractor.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 vdiamant. All rights reserved.
//
import Foundation
class PuzzleTwoInteractor: PuzzleTwoInteractorInput {

    weak var output: PuzzleTwoInteractorOutput!

    // MARK: - PuzzleTwoInteractorInput methods
    
    func loadProgram() {
        
        let program = Program(title: "Tobacco Road",
                              subtitle: "TYLER HANSBOURG, GERLAND HENDERSON JR.",
                              poster: UIImage(named: "dummyPoster")!,
                              episodes: [
                                            Episode(title: "Episode Eleven: Justise Winslow",
                                                    subtitle:  "Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks",
                                                    startTime: Date().addingTimeInterval(60 * 60 * 24 * 6)),
                                            Episode(title: "Episode Eleven: Justise Winslow",
                                                    subtitle:  "Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks",
                                                    startTime: Date().addingTimeInterval(60 * 60 * 24 * 10)),
                                            Episode(title: "Episode Eleven: Justise Winslow",
                                                    subtitle:  "Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks",
                                                    startTime: Date().addingTimeInterval(60 * 60 * 24 * 20)),
                                            Episode(title: "Episode Eleven: Justise Winslow",
                                                    subtitle:  "Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks",
                                                    startTime: Date().addingTimeInterval(60 * 60 * 24 * 50)),
                                            Episode(title: "Episode Eleven: Justise Winslow",
                                                    subtitle:  "Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks",
                                                    startTime: Date().addingTimeInterval(60 * 90 * 24 * 50)),
                                            Episode(title: "Episode Eleven: Justise Winslow",
                                                    subtitle:  "Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks",
                                                    startTime: Date().addingTimeInterval(60 * 60 * 12 * 60)),
                                            Episode(title: "Episode Eleven: Justise Winslow",
                                                    subtitle:  "Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks Gerald and Tyler rexap opening wekend of the NCAA Tournament. Tyler breaks",
                                                    startTime: Date().addingTimeInterval(60 * 60 * 24 * 70))
            ],
                              description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
                              creditsPeople: [
                                            MetaData(firstRowData: "Tyler Hansbrough", secondRowData: "Host"),
                                            MetaData(firstRowData: "Gerald Henderson Jr.", secondRowData: "Host"),
                                            MetaData(firstRowData: "Tyler Hansbrough", secondRowData: "Host"),
                                            MetaData(firstRowData: "Gerald Henderson Jr.", secondRowData: "Host"),
                                            MetaData(firstRowData: "Tyler Hansbrough", secondRowData: "Host"),
                                            MetaData(firstRowData: "Gerald Henderson Jr.", secondRowData: "Host")
            ],
                              links: [
                                            MetaData(firstRowData: "www.testLink1.gr", secondRowData: ""),
                                            MetaData(firstRowData: "www.testLink2.gr", secondRowData: ""),
                                            MetaData(firstRowData: "www.testLink3.gr", secondRowData: ""),
                                            MetaData(firstRowData: "www.testLink4.gr", secondRowData: ""),
                                            MetaData(firstRowData: "www.testLink5.gr", secondRowData: "")
            ])
        
        self.output.didLoadProgramSuccessfully(program: program)
    }
    
}
