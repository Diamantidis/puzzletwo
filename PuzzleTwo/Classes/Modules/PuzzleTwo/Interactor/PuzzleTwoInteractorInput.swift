//
//  PuzzleTwoInteractorInput.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 vdiamant. All rights reserved.
//

import Foundation

protocol PuzzleTwoInteractorInput {
    func loadProgram()
}
