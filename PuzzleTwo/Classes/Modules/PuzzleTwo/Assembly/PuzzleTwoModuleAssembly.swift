//
//  PuzzleTwoModuleAssembly.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 vdiamant. All rights reserved.
//

class PuzzleTwoModuleAssembly {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController, delegate: PuzzleTwoModuleOutput? = nil) -> PuzzleTwoModuleInput? {

        if let viewController = viewInput as? PuzzleTwoViewController {
            return configure(viewController: viewController, delegate: delegate)
        }
        
        return nil
    }

    private func configure(viewController: PuzzleTwoViewController, delegate: PuzzleTwoModuleOutput?) -> PuzzleTwoModuleInput? {

        let router = PuzzleTwoRouter()

        let presenter = PuzzleTwoPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.delegate = delegate

        let interactor = PuzzleTwoInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.theme = PuzzleTwoVCTheme()
        
        return presenter
    }

}
