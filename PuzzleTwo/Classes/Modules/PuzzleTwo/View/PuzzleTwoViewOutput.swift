//
//  PuzzleTwoViewOutput.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 vdiamant. All rights reserved.
//

protocol PuzzleTwoViewOutput {

    func viewIsReady()
    func didClickEpisodesButton()
    func didClickAboutButton()
    func transitionToAnotherSizeCompleted()
}
