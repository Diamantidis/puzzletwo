//
//  PuzzleTwoViewController.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 vdiamant. All rights reserved.
//

struct PuzzleTwoVCTheme {
    let mainTitleTextColor = BaseTheme.mainTitleTextColor
    let secondTitleTextColor = BaseTheme.secondTitleTextColor
    let mainViewBackgroundColor = BaseTheme.mainViewBackgroundColor
    let mainYellowColor = BaseTheme.mainYellowColor
}

class PuzzleTwoViewController: BaseViewController, PuzzleTwoViewInput {

    var output: PuzzleTwoViewOutput!
    var theme: PuzzleTwoVCTheme!
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var posterGradientView: UIGradientView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var subscribeButton: UIButton!
    
    @IBOutlet weak var episodesCollectionView: EpisodesCollectionView!
    @IBOutlet weak var aboutCollectionView: AboutSectionsCollectionView!
    
    @IBOutlet weak var buttonsContainerStackView: UIStackView!
    @IBOutlet weak var episodesButton: UIButton!
    @IBOutlet weak var aboutButton: UIButton!
    @IBOutlet weak var switchRowView: UIView!
    @IBOutlet weak var rowBellowButtonsView: UIView!
    
    
    @IBOutlet weak var constraintToAddHeightForScrollview: NSLayoutConstraint!
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var episodesCollectionViewLeadingContraint: NSLayoutConstraint!
    
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override func localizeStrings() {
        self.subscribeButton.setTitle("SUBSCRIBE", for: .normal)
        self.episodesButton.setTitle("EPISODES", for: .normal)
        self.aboutButton.setTitle("ABOUT", for: .normal)
    }
    
    // MARK: - PuzzleTwoViewInput
    func setupInitialState() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.view.backgroundColor = theme.mainViewBackgroundColor
        self.subscribeButton.backgroundColor = theme.mainYellowColor
        self.titleLabel.textColor = theme.mainTitleTextColor
        self.subtitleLabel.textColor = theme.secondTitleTextColor
        self.buttonsContainerStackView.backgroundColor = theme.mainViewBackgroundColor
        self.episodesButton.backgroundColor = theme.mainViewBackgroundColor
        self.aboutButton.backgroundColor = theme.mainViewBackgroundColor
        self.rowBellowButtonsView.backgroundColor = theme.secondTitleTextColor
        self.switchRowView.backgroundColor = theme.mainYellowColor
        self.posterGradientView.barGradientStartColor = theme.mainViewBackgroundColor
        self.posterGradientView.barGradientEndColor = UIColor.clear
        self.posterGradientView.reloadGradientView()
    }
    
    func updatePosterGradientView() {
        UIView.animate(withDuration: 0.0) {
            self.posterGradientView.reloadGradientView(withFrame: self.posterGradientView.frame)
        }
    }
    
    func updateTitle(title: String) {
        self.titleLabel.text = title
    }
    
    func updateSubtitle(subtitle: String) {
        self.subtitleLabel.text = subtitle
    }
    
    func updatePosterImage(image: UIImage) {
        self.posterImageView.image = image
    }
    
    func updateEpisodes(episodes: [Episode]) {
         self.episodesCollectionView.isHidden = false
         self.episodesCollectionView.episodes = episodes
    }
    
    func updateAboutMetaData(aboutMetaData: AboutMetaData) {
        self.aboutCollectionView.aboutInformation = aboutMetaData
    }
    
    func updateConstraintsValues(withCollectionViewHeight: CGFloat, extraSpaceConstraintValue: CGFloat) {
        UIView.animate(withDuration: 0.0) {
            self.containerViewHeight.constant = withCollectionViewHeight
            self.constraintToAddHeightForScrollview.constant = extraSpaceConstraintValue
            self.view.layoutIfNeeded()
        }
    }
    
    func navigateToAboutCollectionView() {
        UIView.animate(withDuration: 0.15) {
            self.episodesCollectionViewLeadingContraint.constant = -UIScreen.main.bounds.width
            self.view.layoutIfNeeded()
        }
    }
    
    func navigateToEpisodesCollectionView() {
        UIView.animate(withDuration: 0.15) {
            self.episodesCollectionViewLeadingContraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func updateYellowRowToAboutButton() {
        self.updateSwitchRowLabelToMatch(button: self.aboutButton)
    }
    
    func updateYellowRowToEpisodesButton() {
        self.updateSwitchRowLabelToMatch(button: self.episodesButton)
    }
    
    private func updateSwitchRowLabelToMatch(button: UIButton) {
        UIView.animate(withDuration: 0.25) {
            self.switchRowView.frame = CGRect(x: button.frame.origin.x, y: self.switchRowView.frame.origin.y, width: button.frame.width, height: self.switchRowView.frame.height)
            self.view.layoutIfNeeded()
        }
    }
    
    override func transitionToAnotherSizeCompleted() {
        self.output.transitionToAnotherSizeCompleted()
    }
    
    
    
    // MARK: - IBActions
    
    @IBAction func didClickSubscribeButton(_ sender: Any) {
        // Implement me
    }
    
    @IBAction func didClickEpisodesButton(_ sender: Any) {
        self.output.didClickEpisodesButton()
    }
    
    @IBAction func didClickAboutButton(_ sender: Any) {
        self.output.didClickAboutButton()
    }
    
    
    
}
