//
//  PuzzleTwoViewInput.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 vdiamant. All rights reserved.
//

protocol PuzzleTwoViewInput: class {

    func setupInitialState()
    func updateTitle(title: String)
    func updateSubtitle(subtitle: String)
    func updatePosterImage(image: UIImage)
    func updateEpisodes(episodes: [Episode])
    func updateConstraintsValues(withCollectionViewHeight: CGFloat, extraSpaceConstraintValue: CGFloat)
    func updateYellowRowToEpisodesButton()
    func updateYellowRowToAboutButton()
    func updateAboutMetaData(aboutMetaData: AboutMetaData)
    
    func navigateToAboutCollectionView()
    func navigateToEpisodesCollectionView()
    func updatePosterGradientView()
}
