//
//  PuzzleTwoPresenter.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 vdiamant. All rights reserved.
//


enum PuzzleTwoSelectedState {
    case episodes
    case about
}


class PuzzleTwoPresenter: PuzzleTwoModuleInput, PuzzleTwoViewOutput, PuzzleTwoInteractorOutput {

    
    weak var view: PuzzleTwoViewInput!
    var interactor: PuzzleTwoInteractorInput!
    var router: PuzzleTwoRouterInput!
    weak var delegate: PuzzleTwoModuleOutput?

    
    var program: Program?
    var selectedState: PuzzleTwoSelectedState = .episodes
    
	// MARK: - PuzzleTwoModuleInput methods
	

	// MARK: - PuzzleTwoViewOutput methods
    func viewIsReady() {
		view.setupInitialState()
        interactor.loadProgram()
    }
    
    func didClickEpisodesButton() {
        self.selectedState = .episodes
        self.view.updateYellowRowToEpisodesButton()
        self.view.navigateToEpisodesCollectionView()
        self.updateDataForEpisodesCollectionView()
    }
    
    func didClickAboutButton() {
        self.selectedState = .about
        self.view.updateYellowRowToAboutButton()
        self.updateDataForAboutCollectionView()
        self.view.navigateToAboutCollectionView()
    }
    
    func transitionToAnotherSizeCompleted() {
        switch self.selectedState {
            case .episodes:
                self.view.updateYellowRowToEpisodesButton()
                self.view.navigateToEpisodesCollectionView()
                self.updateDataForEpisodesCollectionView()
                break
            case .about:
                self.view.updateYellowRowToAboutButton()
                self.view.navigateToAboutCollectionView()
                self.updateDataForAboutCollectionView()
                break
        }
        self.view.updatePosterGradientView()
    }
    
    // MARK: - PuzzleTwoInteractorOutput methods
    
    func didLoadProgramSuccessfully(program: Program) {
        self.program = program
        
        if let title = program.title {
            self.view.updateTitle(title: title)
        }
        if let subtitle = program.subtitle {
            self.view.updateSubtitle(subtitle: subtitle)
        }
        if let poster = program.poster {
            self.view.updatePosterImage(image: poster)
        }
        
        self.updateDataForEpisodesCollectionView()
    }
    
    // MARK: - Internal Functions
    
    private func updateDataForEpisodesCollectionView() {
        self.view.updateEpisodes(episodes: program?.episodes ?? [])
        self.configureHeightForEpisodesCollectionView(numberOfEpisodes: program?.episodes.count ?? 0)
    }
    
    private func updateDataForAboutCollectionView() {
        if let aboutMetaData = self.program?.aboutMetaData {
            self.view.updateAboutMetaData(aboutMetaData: aboutMetaData)
        }
        self.configureContraintsForAboutMetaDataCollecitonView()
    }
    
    private func configureHeightForEpisodesCollectionView(numberOfEpisodes: Int) {
        let episodesCollectionViewContentHeight:CGFloat = BaseTheme.episodesCollectionViewCellHeight * CGFloat(numberOfEpisodes)
        self.configureContraintsHeightForView(collectionViewHeight: episodesCollectionViewContentHeight)
    }
    
    private func configureContraintsHeightForView(collectionViewHeight: CGFloat) {
        let totalScreenHeight = UIScreen.main.bounds.height
        var extraSpace:CGFloat = (totalScreenHeight - collectionViewHeight - BaseTheme.containerViewContentHeight) * -1
        let computedHeight = collectionViewHeight < 0 ? 0 : collectionViewHeight
        extraSpace = extraSpace < 0 ? 0 : extraSpace
        self.view.updateConstraintsValues(withCollectionViewHeight: computedHeight, extraSpaceConstraintValue: extraSpace)
    }
    
    private func configureContraintsForAboutMetaDataCollecitonView() {
        guard let aboutMetaData = self.program?.aboutMetaData else {
            return
        }
        let aboutCollectionViewContentHeight = aboutMetaData.getHeightForDescription() + aboutMetaData.getHeightForLinks() + aboutMetaData.getHeightForCreditsPeople()
        self.configureContraintsHeightForView(collectionViewHeight: aboutCollectionViewContentHeight)
    }
	
}
