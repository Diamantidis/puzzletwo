//
//  EpisodesCollectionViewCell.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

protocol EpisodesCollectionViewCellOutput: AnyObject {
    
}

class EpisodesCollectionViewCell: UICollectionViewCell {
    
    
    
    //MARK - IBOutlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    //MARK :- Variables
    weak var output: EpisodesCollectionViewCellOutput?
    
    var episode: Episode? {
        didSet {
            if let episode = self.episode {
                self.updateEpisodeDetails(episode: episode)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureUI()
    }
    
    private func configureUI() {
        self.backgroundColor = BaseTheme.mainViewBackgroundColor
        self.dateLabel.textColor = BaseTheme.secondTitleTextColor
        self.titleLabel.textColor = BaseTheme.mainTitleTextColor
        self.subtitleLabel.textColor = BaseTheme.mainTitleTextColor
    }
    
    private func updateEpisodeDetails(episode: Episode) {
        if let title = episode.title {
            self.titleLabel.text = title
        }
        if let subtitle = episode.subtitle {
            self.subtitleLabel.text = subtitle
        }
        if let date = episode.startTime {
            self.dateLabel.text = date.formateMeForEpisodeCells()
        }
        
    }
    
}
