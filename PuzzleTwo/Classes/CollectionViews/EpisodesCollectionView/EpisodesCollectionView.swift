//
//  EpisodesCollectionView.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

protocol EpisodesCollectionViewOutput: AnyObject {
    
}

class EpisodesCollectionView: UICollectionView {
    
    weak var output: EpisodesCollectionViewOutput?
    var episodes: [Episode] = [] {
        didSet {
            self.reloadData()
        }
    }
    
    let cellName: String = "EpisodesCollectionViewCell"
    let cellIdentifier: String = "EpisodesCollectionViewCellIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: cellName, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        self.delegate = self
        self.dataSource = self
    }
}

extension EpisodesCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return episodes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath) as! EpisodesCollectionViewCell
        cell.output = self
        cell.episode = self.episodes[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: BaseTheme.episodesCollectionViewCellHeight)
    }
}

extension EpisodesCollectionView: EpisodesCollectionViewCellOutput {
    
}
