//
//  AboutSectionsCollectionViewCell.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

protocol AboutSectionsCollectionViewCellOutput: AnyObject {
    
}

class AboutSectionsCollectionViewCell: UICollectionViewCell {
    
    weak var output: AboutSectionsCollectionViewCellOutput?
    
    var title: String? {
        didSet {
            if let title = self.title {
                self.titleLabel.text = title
            }
        }
    }
    var collectionViewHeight: CGFloat = BaseTheme.mainMetaDataDoubleCellHeight
    
    var metaData: [MetaData] = [] {
        didSet {
            self.metaDataCollectionView.heightValue = collectionViewHeight
            self.metaDataCollectionView.metaData = metaData
        }
    }
    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var metaDataCollectionView: MetaDataCollectionView!
    @IBOutlet weak var bottomRow: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureUI()
    }
    
    private func configureUI() {
        self.titleLabel.textColor = BaseTheme.secondTitleTextColor
        self.backgroundColor = BaseTheme.mainViewBackgroundColor
        self.bottomRow.backgroundColor = BaseTheme.secondTitleTextColorWithTransparency
    }
    
}
