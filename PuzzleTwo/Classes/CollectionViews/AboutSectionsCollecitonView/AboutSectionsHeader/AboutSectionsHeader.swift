//
//  AboutSectionsHeader.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

import Foundation
class AboutSectionsHeader: UICollectionReusableView {
    
    
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var bottomRow: UIView!
    
    var summary:String? {
        didSet {
            if let summary = summary {
                self.summaryLabel.text = summary
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureUI()
    }
    
    private func configureUI() {
        summaryLabel.textColor = BaseTheme.mainTitleTextColor
        summaryLabel.font = BaseTheme.mainAboutSummaryFont
        self.backgroundColor = BaseTheme.mainViewBackgroundColor
        self.bottomRow.backgroundColor = BaseTheme.secondTitleTextColorWithTransparency
    }
    
}
