//
//  MetaDataCollectionView.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

protocol MetaDataCollectionViewOutput: AnyObject {
    
}

class MetaDataCollectionView: UICollectionView {
    
    let cellName: String = "MetaDataCollectionViewCell"
    let cellIdentifier: String = "MetaDataCollectionViewCellIdentifier"
    
    var heightValue: CGFloat = BaseTheme.mainMetaDataDoubleCellHeight
    var metaData: [MetaData] = [] {
        didSet {
            self.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: cellName, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        self.delegate = self
        self.dataSource = self
    }
}

extension MetaDataCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return metaData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath) as! MetaDataCollectionViewCell
        cell.output = self
        cell.metaData = metaData[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: heightValue)
    }
}

extension MetaDataCollectionView: MetaDataCollectionViewCellOutput {
    
}
