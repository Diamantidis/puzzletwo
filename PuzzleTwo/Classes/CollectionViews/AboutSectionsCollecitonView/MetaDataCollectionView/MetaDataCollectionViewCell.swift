//
//  MetaDataCollectionViewCell.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

protocol MetaDataCollectionViewCellOutput: AnyObject {
    
}

class MetaDataCollectionViewCell: UICollectionViewCell {
    
    weak var output: MetaDataCollectionViewCellOutput?
    
    //MARK :- IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    var metaData: MetaData? {
        didSet {
            if let metaData = metaData {
                self.configurePeopleInfo(metaData: metaData)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureUI()
    }
    
    private func configureUI() {
        self.titleLabel.textColor = BaseTheme.mainTitleTextColor
        self.subtitleLabel.textColor = BaseTheme.secondTitleTextColor
        self.backgroundColor = BaseTheme.mainViewBackgroundColor
    }
    
    private func configurePeopleInfo(metaData: MetaData) {
        if let name = metaData.firstRowData {
            self.titleLabel.text = name
        }
        if let type = metaData.secondRowData {
            self.subtitleLabel.text = type
        }
    }
    
}
