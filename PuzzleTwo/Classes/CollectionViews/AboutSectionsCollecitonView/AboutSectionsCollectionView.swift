//
//  AboutSectionsCollectionView.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

protocol AboutSectionsCollectionViewOutput: AnyObject {
    
}

class AboutSectionsCollectionView: UICollectionView {
    
    weak var output: AboutSectionsCollectionViewOutput?
    var aboutInformation: AboutMetaData? {
        didSet {
            self.reloadData()
        }
    }
    
    let cellName: String = "AboutSectionsCollectionViewCell"
    let cellIdentifier: String = "AboutSectionsCollectionViewCellIdentifier"
    
    let headerName: String = "AboutSectionsHeader"
    let headerIdentifier: String = "AboutSectionsHeaderIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: cellName, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        let headerNib = UINib(nibName: headerName, bundle: nil)
        self.register(headerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: self.headerIdentifier)
        self.delegate = self
        self.dataSource = self
    }
}

extension AboutSectionsCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2 //Implement Check based on requirements
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath) as! AboutSectionsCollectionViewCell
        cell.output = self
        cell.collectionViewHeight = indexPath.row == 0 ? BaseTheme.mainMetaDataDoubleCellHeight : BaseTheme.mainMetaDataSingleCellHeight
        cell.title = indexPath.row == 0 ? "CREDITS" :"LINKS"
        cell.metaData = indexPath.row == 0 ? aboutInformation?.creditsPeople ?? [] : aboutInformation?.links ?? []
        return cell // Fix ME
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = indexPath.row == 0 ? aboutInformation?.getHeightForCreditsPeople() ?? 0 : aboutInformation?.getHeightForLinks() ?? 0
        // If the requirements change and it has more that people and information, this needs to change
        return CGSize(width: collectionView.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: self.headerIdentifier, for: indexPath) as? AboutSectionsHeader else {
            return UICollectionReusableView()
        }
        cell.summary = aboutInformation?.description
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: aboutInformation?.getHeightForDescription() ?? 0) // FIX ME
    }
}

extension AboutSectionsCollectionView: AboutSectionsCollectionViewCellOutput {
    
}
