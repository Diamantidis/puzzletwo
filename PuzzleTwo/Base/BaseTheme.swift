//
//  BaseTheme.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

class BaseTheme {
    
    static let mainTitleTextColor = UIColor.white
    static let secondTitleTextColor = UIColor(red: 187/255, green: 189/255, blue: 186/255, alpha: 1)
    static let secondTitleTextColorWithTransparency = UIColor(red: 187/255, green: 189/255, blue: 186/255, alpha: 1).withAlphaComponent(0.3)
    static let mainViewBackgroundColor = UIColor(red: 31/255, green: 34/255, blue: 31/255, alpha: 1)
    static let mainYellowColor = UIColor(red: 253/255, green: 225/255, blue: 18/255, alpha: 1)
    
    
    static let episodesCollectionViewCellHeight: CGFloat = 130
    static let containerViewContentHeight:CGFloat = 338
    static let mainMetaDataDoubleCellHeight: CGFloat = 70
    static let mainMetaDataSingleCellHeight: CGFloat = 40
    
    static let mainAboutSummaryFont:UIFont = UIFont.systemFont(ofSize: 17.0, weight: .regular)
}
