//
//  BaseViewController.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

class BaseViewController: UIViewController {
    
    // TODO: - Implement Base functionality
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizeStrings()
    }
    
    func localizeStrings() {
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { (context) in
            // Implement will transition To if needed
        }, completion:{ (context) in
            self.transitionToAnotherSizeCompleted()
        })
    }
    
    func transitionToAnotherSizeCompleted() {
        
    }
    
}
