//
//  AboutMetaData.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

import Foundation
class AboutMetaData {
    
    var description: String?
    var creditsPeople:[MetaData] = []
    var links: [MetaData] = []
    
    init(description:String, creditsPeople: [MetaData], links: [MetaData]) {
        self.description = description
        self.creditsPeople = creditsPeople
        self.links = links
    }
    
    
    func getHeightForDescription() -> CGFloat {
        return (description?.height(withConstrainedWidth: UIScreen.main.bounds.width - 16 - 16, font: BaseTheme.mainAboutSummaryFont) ?? 0) + 16 + 16
    }
    
    func getHeightForCreditsPeople() -> CGFloat {
        return BaseTheme.mainMetaDataDoubleCellHeight * CGFloat(creditsPeople.count) + 18 + 12 + 8
    }
    
    func getHeightForLinks() -> CGFloat {
        return BaseTheme.mainMetaDataSingleCellHeight * CGFloat(links.count) + 18 + 12 + 8
    }
    
    
}
