//
//  MetaData.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

import Foundation
class MetaData {
    
    var firstRowData: String?
    var secondRowData: String?
    
    init(firstRowData: String, secondRowData: String) {
        self.firstRowData = firstRowData
        self.secondRowData = secondRowData
    }
}
