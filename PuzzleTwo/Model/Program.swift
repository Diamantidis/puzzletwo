//
//  Program.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

import Foundation
class Program {
    
    var title: String?
    var subtitle: String?
    var episodes: [Episode] = []
    var aboutMetaData: AboutMetaData?
    var poster: UIImage?
    
    init(title: String, subtitle: String, poster: UIImage, episodes: [Episode], description:String, creditsPeople: [MetaData], links: [MetaData]) {
        self.title = title
        self.subtitle = subtitle
        self.poster = poster
        self.episodes = episodes
        self.aboutMetaData = AboutMetaData(description: description, creditsPeople: creditsPeople, links: links)
    }
    
}
