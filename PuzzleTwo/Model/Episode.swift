//
//  Episode.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

import Foundation
class Episode {
    var startTime: Date?
    var title: String?
    var subtitle: String?
    
    init(title: String, subtitle: String, startTime: Date) {
        self.title = title
        self.subtitle = subtitle
        self.startTime = startTime
    }
    
}
