//
//  Date+Extensions.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

import Foundation

extension Date {
    
    func formateMeForEpisodeCells() -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MMMM d • HH"
        var dateRepresentationString = dateFormater.string(from: self).uppercased()
        dateRepresentationString += "h "
        dateFormater.dateFormat = "mm"
        dateRepresentationString += dateFormater.string(from: self)
        dateRepresentationString += "m"
        return dateRepresentationString
    }
    
}
