//
//  UIGradientView.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

class UIGradientView: UIView {
    
    @IBInspectable var barGradientStartColor : UIColor = UIColor.init(white: 0.0, alpha: 1.0)
    @IBInspectable var barGradientEndColor : UIColor = UIColor.init(white: 0.0, alpha: 0.0)
    @IBInspectable var startPoint : CGPoint = CGPoint(x: 0, y: 0)
    @IBInspectable var endPoint : CGPoint = CGPoint(x: 0, y: 1)
    
    private var isGradientHasBeenAdded = false
    
    private var gradientLayer : CAGradientLayer!
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !isGradientHasBeenAdded {
            addGradient(withFrame: self.bounds)
            isGradientHasBeenAdded = true
        }
        
        gradientLayer.frame = self.bounds
    }
    
    func addGradient(withFrame: CGRect){
        gradientLayer = CAGradientLayer()
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        let colors = [barGradientEndColor.cgColor,
                      barGradientStartColor.cgColor]
        gradientLayer.colors = colors
        gradientLayer.isOpaque = false
        gradientLayer.frame = withFrame
        self.layer.insertSublayer(gradientLayer, at: 0)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func reloadGradientView() {
        self.gradientLayer?.removeFromSuperlayer()
        self.addGradient(withFrame: self.bounds)
    }
    
    func reloadGradientView(withFrame: CGRect) {
        self.gradientLayer?.removeFromSuperlayer()
        for layer in (self.layer.sublayers ?? []) {
            layer.isHidden = true
            layer.removeFromSuperlayer()
        }
        self.addGradient(withFrame: withFrame)
    }
    
}
