//
//  PuzzleTwoModuleAssemblyTests.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 vdiamant. All rights reserved.
//

import XCTest

@testable import PuzzleTwo

class PuzzleTwoModuleAssemblyTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        // given
        let viewController = PuzzleTwoViewController()
        let assembly = PuzzleTwoModuleAssembly()

        // when
        assembly.configureModuleForViewInput(viewInput: viewController)

        // then
		
        XCTAssertNotNil(viewController.output, "output in PuzzleTwoViewController is nil after configuration")
        XCTAssertTrue(viewController.output is PuzzleTwoPresenter, "output is not PuzzleTwoPresenter")
        XCTAssertNotNil(viewController.theme, "theme in PuzzleTwoViewController is nil after configuration")
        
        let presenter: PuzzleTwoPresenter = viewController.output as! PuzzleTwoPresenter
        XCTAssertNotNil(presenter.view, "view in PuzzleTwoPresenter is nil after configuration")
        XCTAssertNotNil(presenter.interactor, "interactor in PuzzleTwoPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in PuzzleTwoPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is PuzzleTwoRouter, "router is not PuzzleTwoRouter")

        let interactor: PuzzleTwoInteractor = presenter.interactor as! PuzzleTwoInteractor
        XCTAssertNotNil(interactor.output, "output in PuzzleTwoInteractor is nil after configuration")
    }
}
