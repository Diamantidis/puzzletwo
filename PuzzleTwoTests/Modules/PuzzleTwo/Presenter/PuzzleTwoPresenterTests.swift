//
//  PuzzleTwoPresenterTests.swift
//  PuzzleTwo
//
//  Created by Vasileios Diamantidis on 12/10/2019.
//  Copyright © 2019 vdiamant. All rights reserved.
//

import XCTest

@testable import PuzzleTwo

class PuzzleTwoPresenterTest: XCTestCase {

    var presenter : PuzzleTwoPresenter!
//     var view : MockPuzzleTwoViewInput!
//     var router : MockPuzzleTwoRouterInput!
//     var interactor : MockPuzzleTwoInteractorInput!

//${PODS_ROOT}/Cuckoo/run generate --testable "${PROJECT_NAME}" \
//--output "./${PROJECT_NAME}Tests/Modules/PuzzleTwo/PuzzleTwoMocks.swift" \
//"${INPUT_MODULES_DIR}/PuzzleTwo/View/PuzzleTwoViewInput.swift" \
//"${INPUT_MODULES_DIR}/PuzzleTwo/Router/PuzzleTwoRouterInput.swift" \
//"${INPUT_MODULES_DIR}/PuzzleTwo/Interactor/PuzzleTwoInteractorInput.swift" \
//"${INPUT_MODULES_DIR}/PuzzleTwo/Interactor/PuzzleTwoInteractorOutput.swift" \

    override func setUp() {
        super.setUp()
        
        presenter = PuzzleTwoPresenter()
//        view = MockPuzzleTwoViewInput()
//        router = MockPuzzleTwoRouterInput()
//        interactor = MockPuzzleTwoInteractorInput()
//        presenter.view = view
//        presenter.router = router
//        presenter.interactor = interactor
    }

    override func tearDown() {
//		view = nil
//		router = nil
//		interactor = nil
		presenter = nil

        super.tearDown()
    }
}
